import { Database } from '../databases/database_abstract';
import { DatabaseInstanceStrategy } from '../database';

export class PassengerService {
    private readonly _db: Database;

    constructor() {
        this._db = DatabaseInstanceStrategy.getInstance();
    }

    public async linkPassengerToFlight(passenger_id : number, flightCode: string) {
        return this._db.linkPassengerToFlight(passenger_id, flightCode);
    }

    public async getPassengers() {
        return this._db.getPassengers();
    }
}
