import {
    JsonController,
    Get,
    Body,
    Patch,
    Param,
} from 'routing-controllers';
import { PassengerService } from '../services/passenger.service';


@JsonController('/passenger', { transformResponse: false })
export default class PassengerController {
    private _passengerService: PassengerService;

    constructor() {
        this._passengerService = new PassengerService();
    }

    @Get('')
    async getAll() {
        return {
            status: 200,
            data: await this._passengerService.getPassengers(),
        };
    }

    // link passenger to flight
    @Patch('/:passenger_id')
    async linkPassengerToFlight(
        @Body() reqBody: any,
        @Param('passenger_id') passenger_id: number
    ) {
        return {
            status: 200,
            data: await this._passengerService.linkPassengerToFlight(passenger_id, reqBody.flight_code),
        };
    }
}
