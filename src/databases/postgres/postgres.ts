import { Database } from '../database_abstract';

import { newDb, IMemoryDb } from 'pg-mem';

export class PostgreStrategy extends Database {
    _instance: IMemoryDb;

    constructor() {
        super();
        this.getInstance();
    }

    private async getInstance() {
        const db = newDb();

        db.public.many(`
            CREATE TABLE flights (
                code VARCHAR(5) PRIMARY KEY,
                origin VARCHAR(50),
                destination VARCHAR(50),
                status VARCHAR(50)
            );
        `);

        db.public.many(`
            CREATE TABLE passengers (
                id bigint PRIMARY KEY,
                name VARCHAR(50)
            );
        `);

        db.public.many(`
            CREATE TABLE passenger_flight (
                passenger_id bigint references passengers(id),
                flight_code VARCHAR(5) references flights(code)
            );
        `);

        db.public.many(`
            INSERT INTO flights (code, origin, destination, status)
            VALUES ('LH123', 'Frankfurt', 'New York', 'on time'),
                     ('LH124', 'Frankfurt', 'New York', 'delayed'),
                        ('LH125', 'Frankfurt', 'New York', 'on time')
        `);
        
        db.public.many(`
            INSERT INTO passengers (id, name)
            VALUES (3000, 'Passenger 1'),
                (3001, 'Passenger 2'),
                (3002, 'Passenger 3')
        `);

        PostgreStrategy._instance = db;

        return db;
    }

    public async getFlights() {
        return PostgreStrategy._instance.public.many('SELECT * FROM flights');
    }

    public async addFlight(flight: {
        code: string;
        origin: string;
        destination: string;
        status: string;
    }) {
        return PostgreStrategy._instance.public.many(
            `INSERT INTO flights (code, origin, destination, status) VALUES ('${flight.code}', '${flight.origin}', '${flight.destination}', '${flight.status}')`,
        );
    }

    public async getPassengers() {
        return PostgreStrategy._instance.public.many('SELECT * FROM passengers p LEFT JOIN passenger_flight pf ON p.id = pf.passenger_id');
    }

    public async linkPassengerToFlight(passenger_id : number, flightCode: string) {
        return PostgreStrategy._instance.public.many(
            `INSERT INTO passenger_flight (passenger_id, flight_code) VALUES ('${passenger_id}', '${flightCode}')`,
        );
    }
}
